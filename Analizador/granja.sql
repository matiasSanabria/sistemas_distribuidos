--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: climas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE climas (
    id_clima integer NOT NULL,
    zona text,
    departamento text,
    distrito text,
    anho numeric,
    mes numeric,
    dia numeric,
    hora numeric,
    minuto numeric,
    segundo numeric
);


ALTER TABLE climas OWNER TO postgres;

--
-- Name: clima_id_clima_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE clima_id_clima_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE clima_id_clima_seq OWNER TO postgres;

--
-- Name: clima_id_clima_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE clima_id_clima_seq OWNED BY climas.id_clima;


--
-- Name: cotizaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cotizaciones (
    id_cotizacion integer NOT NULL,
    "precio-kg" text,
    dia text
);


ALTER TABLE cotizaciones OWNER TO postgres;

--
-- Name: cotizacion_id_cotizacion_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cotizacion_id_cotizacion_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cotizacion_id_cotizacion_seq OWNER TO postgres;

--
-- Name: cotizacion_id_cotizacion_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cotizacion_id_cotizacion_seq OWNED BY cotizaciones.id_cotizacion;


--
-- Name: satelite; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE satelite (
    id_satelite integer NOT NULL,
    codigo_cultivo text,
    departamento text,
    distrito text,
    posicion_x text,
    posicion_y text,
    "base64-imagen" text,
    anho numeric,
    mes numeric,
    dia numeric,
    hora numeric,
    minuto numeric,
    segundo numeric
);


ALTER TABLE satelite OWNER TO postgres;

--
-- Name: satelite_id_satelite_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE satelite_id_satelite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE satelite_id_satelite_seq OWNER TO postgres;

--
-- Name: satelite_id_satelite_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE satelite_id_satelite_seq OWNED BY satelite.id_satelite;


--
-- Name: tractor; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tractor (
    id_tractor integer NOT NULL,
    chapa_tractor text,
    altura integer,
    humedad integer,
    peso integer,
    temperatura integer,
    anho numeric,
    mes numeric,
    dia numeric,
    hora numeric,
    minuto numeric,
    segundo numeric
);


ALTER TABLE tractor OWNER TO postgres;

--
-- Name: tractor_id_tractor_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tractor_id_tractor_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE tractor_id_tractor_seq OWNER TO postgres;

--
-- Name: tractor_id_tractor_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tractor_id_tractor_seq OWNED BY tractor.id_tractor;


--
-- Name: id_clima; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY climas ALTER COLUMN id_clima SET DEFAULT nextval('clima_id_clima_seq'::regclass);


--
-- Name: id_cotizacion; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones ALTER COLUMN id_cotizacion SET DEFAULT nextval('cotizacion_id_cotizacion_seq'::regclass);


--
-- Name: id_satelite; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY satelite ALTER COLUMN id_satelite SET DEFAULT nextval('satelite_id_satelite_seq'::regclass);


--
-- Name: id_tractor; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tractor ALTER COLUMN id_tractor SET DEFAULT nextval('tractor_id_tractor_seq'::regclass);


--
-- Name: clima_id_clima_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('clima_id_clima_seq', 1, true);


--
-- Data for Name: climas; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO climas VALUES (1, 'Urbana', 'Central', 'Metropolitana', 2016, 3, 29, 17, 4, 0);


--
-- Name: cotizacion_id_cotizacion_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cotizacion_id_cotizacion_seq', 1, false);


--
-- Data for Name: cotizaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--



--
-- Data for Name: satelite; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO satelite VALUES (1, '3', 'Central', 'Metropolitana', '5', '6', '5465', 2016, 3, 29, 17, 0, 0);


--
-- Name: satelite_id_satelite_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('satelite_id_satelite_seq', 1, true);


--
-- Data for Name: tractor; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO tractor VALUES (2, 'APX-689', 10, 15, 20, 25, 2016, 3, 29, 15, 14, 0);
INSERT INTO tractor VALUES (1, 'AUI-135', 15, 30, 16, 14, 2016, 3, 29, 15, 18, 0);
INSERT INTO tractor VALUES (3, 'APX-689', 10, 15, 20, 25, 2016, 3, 29, 15, 15, 0);
INSERT INTO tractor VALUES (4, 'APX-689', 10, 15, 20, 25, 2016, 3, 29, 15, 16, 0);


--
-- Name: tractor_id_tractor_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tractor_id_tractor_seq', 4, true);


--
-- Name: clima_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY climas
    ADD CONSTRAINT clima_pkey PRIMARY KEY (id_clima);


--
-- Name: cotizacion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cotizaciones
    ADD CONSTRAINT cotizacion_pkey PRIMARY KEY (id_cotizacion);


--
-- Name: satelite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY satelite
    ADD CONSTRAINT satelite_pkey PRIMARY KEY (id_satelite);


--
-- Name: tractor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tractor
    ADD CONSTRAINT tractor_pkey PRIMARY KEY (id_tractor);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
