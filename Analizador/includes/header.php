<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width">
        <title>SDGRANJA</title>
        <link href="css/global-demo.css" rel="stylesheet" />
        <link href='css/cssOswald.css' rel='stylesheet' type='text/css'>
        <link href="css/cssRoboto.css" rel="stylesheet" type="text/css">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <style type="text/css">
            * {
                margin: 0;
                padding: 0;
            }
            body {
                background: url(images/noise_light-grey.jpg);
                font-family: 'Helvetica Neue', arial, sans-serif;
                font-weight: 300;
            }

            h1 {
                font-family: 'Oswald', sans-serif;
                font-size: 4em;
                font-weight: 400;
                margin: 0 0 20px;
                text-align: center;
                text-shadow: 1px 1px 0 #fff, 2px 2px 0 #bbb;
            }
            h2 {
                font-family: 'Oswald', sans-serif;
                font-size: 2em;
                font-weight: 700;
                letter-spacing: -1px;
                margin: 0 0 20px;
                text-align: center;
                text-transform: uppercase;
            }
            hr {
                border-top: 1px solid #ccc;
                border-bottom: 1px solid #fff;
                margin: 25px 0;
                clear: both;
            }
            .centered {
                text-align: center;
            }
            .wrapper {
                width: 100%;
                padding: 30px 0;
            }
            .container {
                width: 1200px;
                margin: 0 auto;
            }
            .table-container {
                padding: 20px;
                margin: 0 0 20px;
                background: #fff;
                box-shadow: 0 0 5px #ddd;
            }
            /* responsive tables */
            .responsive-stacked-table {
                width: 100%;
                border: 1px solid #ddd;
                border-collapse: collapse;
                table-layout: fixed;
            }
            .responsive-stacked-table th,
            .responsive-stacked-table td {
                padding: 10px;
                border-top: 1px solid #ddd;
            }
            .responsive-stacked-table thead {
                background: #eee;
                border-bottom: 3px solid #ddd;
            }
            .responsive-stacked-table tr:nth-child(even) {
                background: #f5f5f5;
            }
            .responsive-stacked-table .fa {
                margin-right: 5px;
            }
            .responsive-stacked-table .fa-check-circle {
                color: #690;
            }
            .responsive-stacked-table .fa-times-circle {
                color: #c00;
            }

            .responsive-stacked-table.with-mobile-labels {
                font-size: .85em;
            }

            @media (max-width: 1199px) {
                .container {
                    width: auto;
                    padding: 0 10px;
                }
            }

            @media (max-width: 767px) {
                .responsive-stacked-table thead {
                    display: none;
                }
                .responsive-stacked-table tr,
                .responsive-stacked-table th,
                .responsive-stacked-table td {
                    display: block;
                }
                .responsive-stacked-table td {
                    border-top: none;
                }
                .responsive-stacked-table tr td:first-child {
                    border-top: 1px solid #ddd;
                    font-weight: bold;
                }
                .responsive-stacked-table.with-mobile-labels tr td:first-child {
                    font-weight: 300;
                }
                .responsive-stacked-table.with-mobile-labels td:before {
                    display: block;
                    font-weight: bold;
                }
                .responsive-stacked-table.with-mobile-labels td:nth-of-type(1):before {
                    content: "Product:";
                }
                .responsive-stacked-table.with-mobile-labels td:nth-of-type(2):before {
                    content: "Processor:";
                }
                .responsive-stacked-table.with-mobile-labels td:nth-of-type(3):before {
                    content: "Memory:";
                }
                .responsive-stacked-table.with-mobile-labels td:nth-of-type(4):before {
                    content: "Hard Drive:";
                }
                .responsive-stacked-table.with-mobile-labels td:nth-of-type(5):before {
                    content: "Graphics Card:";
                }
            }
        </style>
    </head>
    <body>

        <div class="global-header">
            <div class="container">
                <p class="brand"><a href="#">SD<strong>GRANJA</strong></a></p>
                <p class="back-to-article"><a href="javascript:alert('Estamos trabajando arduamente para saber que poner aca, ayudaaaa!!!');">Ayuda &rsaquo;</a></p>
            </div>
        </div>
        <!--/.global-header-->
