<?php
include 'includes/header.php';
include 'includes/conexion.php';
$intervalo = 5;
$hoy = getdate();
/*
  echo '<pre>';
  print_r($hoy);
  echo $hoy['year'] . '/' . $hoy['mon'] . '/' . $hoy['mday'] . ' ' . $hoy['hours'] .':'. $hoy['minutes'] .':'. $hoy['seconds'];
 */
?>



<div class="wrapper">
    <div class="container">
        <h1>Datos de Sensores Remotos</h1>
        <!--table-container-->
        <div class="table-container">
            <h2>Tractor</h2>
            <table class="responsive-stacked-table">
                <thead>
                    <tr>
                        <th>Chapa</th>
                        <th>Altura</th>
                        <th>Humedad</th>
                        <th>Peso</th>
                        <th>Temperatura</th>
                        <th>Estado</th>

                    </tr>
                </thead>
                <tbody>
                    <!-- comienza ciclo -->
                    <?php
                    $q = "select * from tractor";
                    $q = pg_exec($con, $q);
                    while ($row = pg_fetch_row($q)) {
                        echo '<tr>';
                        echo '<td>' . $row['1'] . '</td>
                        <td>' . $row['2'] . ' m.</td>
                        <td>' . $row['3'] . '%</td>
                        <td>' . $row['4'] . ' kg</td>';
                        echo '<td>' . $row['5'] . '&#176;</td>';
                        $r = ($hoy['year'] - $row['6']) * 12 * 30 * 24 * 60 + ($hoy['mon'] - $row['7']) * 30 * 24 * 60 + ($hoy['mday'] - $row['8']) * 24 * 60 + ($hoy['hours'] - $row['9']) * 60 + $hoy['minutes'] - $row['10'];
                        //echo $r;
                        if ($r < $intervalo) {
                            echo '<td><i class="fa fa-check-circle"></i> En linea <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        } else {
                            echo '<td><i class="fa fa-times-circle"></i> Desconectado <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                    <!-- termina ciclo -->
                </tbody>
            </table>
        </div>
        <!--table-SATELITE-->
        <div class="table-container">
            <h2>Satelite</h2>
            <table class="responsive-stacked-table">
                <thead>
                    <tr>
                        <th>C&oacute;d. Cultivo</th>
                        <th>Departamento</th>
                        <th>Distrito</th>
                        <th>Posici&oacute;n</th>
                        <th>Im&aacute;gen</th>
                        <th>Estado</th>

                    </tr>
                </thead>
                <tbody>
                    <!-- comienza ciclo -->
                    <?php
                    $q = "select * from satelite";
                    $q = pg_exec($con, $q);
                    while ($row = pg_fetch_row($q)) {
                        echo '<tr>';
                        echo '<td>' . $row['1'] . '</td>
                        <td>' . $row['2'] . '</td>
                        <td>' . $row['3'] . '</td>
                        <td>' . $row['4'] . ' ' . $row['5'] . ' </td>';
                        echo '<td>' . $row['6'] . '</td>';
                        $r = ($hoy['year'] - $row['7']) * 12 * 30 * 24 * 60 + ($hoy['mon'] - $row['8']) * 30 * 24 * 60 + ($hoy['mday'] - $row['9']) * 24 * 60 + ($hoy['hours'] - $row['10']) * 60 + $hoy['minutes'] - $row['11'];
                        //echo $r;
                        if ($r < $intervalo) {
                            echo '<td><i class="fa fa-check-circle"></i> En linea <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        } else {
                            echo '<td><i class="fa fa-times-circle"></i> Desconectado <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                    <!-- termina ciclo -->
                </tbody>
            </table>
        </div>
        
        
        <!--table-CLIMA-->
        <div class="table-container">
            <h2>Clima</h2>
            <table class="responsive-stacked-table">
                <thead>
                    <tr>
                        <th>Zona</th>
                        <th>Departamento</th>
                        <th>Distrito</th>
                        <th>D&iacute;a/Mes/A&ntilde;o</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- comienza ciclo -->
                    <?php
                    $q = "select * from climas";
                    $q = pg_exec($con, $q);
                    while ($row = pg_fetch_row($q)) {
                        echo '<tr>';
                        echo '<td>' . $row['1'] . '</td>
                        <td>' . $row['2'] . '</td>
                        <td>' . $row['3'] . '</td>
                        <td>' . $row['6'] . '/' . $row['5'] . '/' . $row['4'] . '</td>';
                        $r = ($hoy['year'] - $row['4']) * 12 * 30 * 24 * 60 + ($hoy['mon'] - $row['5']) * 30 * 24 * 60 + ($hoy['mday'] - $row['6']) * 24 * 60 + ($hoy['hours'] - $row['7']) * 60 + $hoy['minutes'] - $row['8'];
                        //echo $r;
                        if ($r < $intervalo) {
                            echo '<td><i class="fa fa-check-circle"></i> En linea <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        } else {
                            echo '<td><i class="fa fa-times-circle"></i> Desconectado <i>(' . $r . ' min. atr&aacute;s)</i></td>';
                        }
                        echo '</tr>';
                    }
                    ?>
                    <!-- termina ciclo -->
                </tbody>
            </table>
        </div>

        
        
        
        
        
        
        
        
        
        <!-- 
        <div class="table-container">
            <h2>Tractores</h2>
            <table class="responsive-stacked-table with-mobile-labels">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Processor</th>
                        <th>Memory</th>
                        <th>Hard Drive</th>
                        <th>Graphics Card</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Macbook Air 11"</td>
                        <td>1.4GHz dual-core Intel Core i5</td>
                        <td>4GB</td>
                        <td>128GB Flash storage</td>
                        <td>Intel HD Graphics 5000</td>
                    </tr>
                    <tr>
                        <td>Macbook Air 13"</td>
                        <td>1.4GHz dual-core Intel Core i5</td>
                        <td>4GB</td>
                        <td>128GB Flash storage</td>
                        <td>Intel HD Graphics 5000</td>
                    </tr>
                    <tr>
                        <td>Macbook Pro 13"</td>
                        <td>2.6GHz dual-core Intel Core i5</td>
                        <td>8GB</td>
                        <td>128GB Flash storage</td>
                        <td>Intel Iris Graphics</td>
                    </tr>
                    <tr>
                        <td>Macbook Pro 15"</td>
                        <td>2.2GHz quad-core Intel Core i7</td>
                        <td>16GB</td>
                        <td>256GB Flash storage</td>
                        <td>Intel Iris Pro Graphics</td>
                    </tr>
                    <tr>
                        <td>iMac 21.5"</td>
                        <td>1.4GHz dual-core Intel Core i5</td>
                        <td>8GB</td>
                        <td>500GB</td>
                        <td>Intel HD Graphics 5000</td>
                    </tr>
                    <tr>
                        <td>iMac 27"</td>
                        <td>3.2GHz quad-core Intel Core i5</td>
                        <td>8GB</td>
                        <td>1TB</td>
                        <td>NVIDIA GeForce GT 755M with 1GB video memory</td>
                    </tr>
                </tbody>
            </table>
        </div>
        tabla tipo 2 -->
        <?php
        include "includes/footer.php";
        ?>
