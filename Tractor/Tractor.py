#!/usr/bin/env python3

import argparse
import datetime
import json
import logging
import random
import socket
import time
import traceback


TCP_IP = ''
TCP_PORT = 10000
BUFFER_SIZE = 1024
TIME_OUT = 5
CODIGO_SEGURIDAD = "Zn4Ko9fn"
TIEMPO_ESPERA_REENVIO = 5
ALFABETO = 'ABCDEFGHIJKLMNOPQRSTUVWXZ'
# Puertos Broadcast (BC)
SEND_BC_PORT = TCP_PORT+1
RECEIVE_BC_PORT = TCP_PORT+2


def funcion_descubrimiento_automatico():
    try:
        broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        broadcast_socket.settimeout(TIME_OUT)
        broadcast_socket.bind(('',RECEIVE_BC_PORT))
        
        broadcast_socket.sendto(CODIGO_SEGURIDAD.encode(),('<broadcast>',SEND_BC_PORT))
        
        data, server_address = broadcast_socket.recvfrom(BUFFER_SIZE)

        return server_address[0]
        
    except OSError as e:
        if(str(e) == 'timed out'):
            print('No se pudo encontrar un servidor en la red.')
            print('Intentando conectarse localmente.')

        logging.exception(traceback.format_exc())
        
        return None


def generarChapa():
    
    return '{0}{1}{2}{3}{4}{5}'.format(random.choice(ALFABETO),
                                        random.choice(ALFABETO),
                                        random.choice(ALFABETO),
                                        random.randint(0,9),
                                        random.randint(0,9),
                                        random.randint(0,9))


# Codigo para el uso de argumentos por linea de comandos
def configurarArgumentos():
        
    parser = argparse.ArgumentParser(description='Cliente Tractor')

    parser.add_argument('-ip', 
                        default=TCP_IP, 
                        help='Direccion IP del servidor')
    parser.add_argument('-p', 
                        default=TCP_PORT, 
                        type=int, 
                        help='Puerto del servicio',
                        dest='tcp_port')
    parser.add_argument('-a', 
                        default=False, 
                        action='store_true', 
                        help='Habilita el descubrimiento automatico del Servidor en la red',
                        dest='descubrimiento_automatico')
    
    return parser

try:
        
    logging.basicConfig(filename='tractor.log', 
        format='%(levelname)s: %(asctime)s\n%(message)s',
        level=logging.INFO)


    # Leer los argumentos de consola de comando
    parser = configurarArgumentos()
    args = parser.parse_args()
    tcp_ip = args.ip
    tcp_port = args.tcp_port
    descubrimiento_automatico = args.descubrimiento_automatico


    if(descubrimiento_automatico == True):
        tcp_ip = funcion_descubrimiento_automatico()
        
        # Si hubo algun error, se setea a la ip por defecto
        if(tcp_ip is None):
            tcp_ip = TCP_IP


    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((tcp_ip, tcp_port))
    
    chapa = generarChapa()

    while True:

        altura = random.randint(0,1000)
        humedad = random.randint(0,100)
        peso = random.randint(1000,5000)
        temperatura = random.randint(-10,50)
        fechatiempo = str(datetime.datetime.now())
        
        message = json.dumps({'tipo': 'tractor', 
                              'chapa': chapa, 
                              'altura': altura, 
                              'humedad': humedad, 
                              'peso': peso, 
                              'temperatura': temperatura,
                              'fechatiempo': fechatiempo})

        client_socket.send(message.encode())
        
        print(message)
        
        time.sleep(TIEMPO_ESPERA_REENVIO)
    
    
except KeyboardInterrupt as e:
    print()
    print('Terminacion manual con Ctrl-C')
    
except ConnectionRefusedError as e:
    if(str(e) == '[Errno 111] Connection refused'):
        print('No se pudo establecer una conexion con el servidor')
    
    logging.exception(traceback.format_exc())

except OSError as e:
    if(str(e) == '[Errno 32] Broken pipe'):
        print('Se interrumpio la conexion con el servidor')
        
    logging.exception(traceback.format_exc())
    
finally:
    client_socket.close()

