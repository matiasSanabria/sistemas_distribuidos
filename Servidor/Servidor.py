#!/usr/bin/env python3

import argparse
import json
import logging
import socket
import sqlite3
import threading
import time
import traceback



TCP_IP = ''
TCP_PORT = 10000
COTIZACION_PORT = TCP_PORT+3
BUFFER_SIZE = 4096
TIME_OUT = 5
CODIGO_SEGURIDAD = "Zn4Ko9fn"
TIEMPO_ESPERA_REENVIO = 5
# Puertos Broadcast (BC)
RECEIVE_BC_PORT = TCP_PORT+1
SEND_BC_PORT = TCP_PORT+2


class ClientThread(threading.Thread):

    def __init__(self, client_socket, address):
        threading.Thread.__init__(self)
        self.client_socket = client_socket
        self.address = address

    def run(self):
        try:
            
            db_conn = sqlite3.connect('granja.db')
            db_cursor = db_conn.cursor()
            
            while True:
                bytes_data = self.client_socket.recv(BUFFER_SIZE)
                if not bytes_data: 
                    self.client_socket.close()
                    return
                    
                json_data = bytes_data.decode('utf-8')
                data = json.loads(json_data)
                        
                if data['tipo'] == 'tractor':
                    t = (data['chapa'], data['altura'], data['humedad'], 
                        data['peso'], data['temperatura'], data['fechatiempo'])
                    db_cursor.execute('insert into tractor values (?,?,?,?,?,?)', t)
                    
                elif data['tipo'] == 'satelite':
                    t = (data['codigo_cultivo'], data['departamento'], 
                        data['distrito'], data['posicio_x'], 
                        data['posicio_y'], data['imagen_base64'],
                        data['fechatiempo'])
                    db_cursor.execute('insert into satelite values (?,?,?,?,?,?,?)', t)
                    
                elif data['tipo'] == 'clima':
                    t = (data['zona'], data['departamento'], 
                        data['distrito'], data['fechatiempo'])
                    db_cursor.execute('insert into clima values (?,?,?,?)', t)
                
                db_conn.commit()
                
                print(data)
                
        except Exception as e:
              logging.exception(traceback.format_exc())
              
        finally:
            self.client_socket.close()
            db_cursor.close()
            db_conn.close()


class ListenBroadcastThread(threading.Thread):
    
    def run(self):
        
        try:
        
            broadcast_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            broadcast_socket.bind(('',RECEIVE_BC_PORT))
            broadcast_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            
            while True:
                try:
                    encoded_data, address = broadcast_socket.recvfrom(BUFFER_SIZE)
                    data = encoded_data.decode('utf-8')
                    
                    if data.startswith(CODIGO_SEGURIDAD):
                        broadcast_socket.sendto(b'', ('<broadcast>', SEND_BC_PORT))
                        
                except Exception as e:
                    logging.exception(traceback.format_exc())
                    
        except Exception as e:
            logging.exception(traceback.format_exc())
            
        finally:
            broadcast_socket.close()

class ConsultaCotizacionThread(threading.Thread):
    
    def run(self):
        
        try:
            
            db_conn = sqlite3.connect('granja.db')
            db_cursor = db_conn.cursor()
            
            cotizacion_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            cotizacion_socket.connect(('', COTIZACION_PORT))
            
            while True:
                cotizacion_socket.send(b'Solicitud de datos')
                encoded_data, server_address = cotizacion_socket.recvfrom(BUFFER_SIZE)
                
                t = (data['moneda'], data['valor'], data['fechatiempo'])
                db_cursor.execute('insert into cotizacion values (?,?,?)', t)
                
                db_conn.commit()
                
                time.sleep(TIEMPO_ESPERA_REENVIO)
                
        except Exception as e:
              logging.exception(traceback.format_exc())
              
        finally:
            cotizacion_socket.close()
            db_cursor.close()
            db_conn.close()
        

try:
    logging.basicConfig(filename='servidor.log', 
        format='%(levelname)s: %(asctime)s\n%(message)s',
        level=logging.INFO)
    
    db_conn = sqlite3.connect('granja.db')
    db_cursor = db_conn.cursor()
    db_cursor.execute('''CREATE TABLE IF NOT EXISTS tractor 
             (chapa text, altura integer, humedad integer, 
             peso integer, temperatura integer,
             fechatiempo text)''')
    db_cursor.execute('''CREATE TABLE IF NOT EXISTS satelite 
             (codigo_cultivo integer, departamento integer, 
             distrito integer, posicio_x integer, posicio_y integer, 
             imagen_base64 text, fechatiempo text)''')
    db_cursor.execute('''CREATE TABLE IF NOT EXISTS clima 
             (zona integer, departamento integer, distrito integer,
             fechatiempo text)''')
    db_cursor.execute('''CREATE TABLE IF NOT EXISTS cotizacion 
             (moneda text, valor real, fechatiempo text)''')
    db_conn.commit()
    db_cursor.close()
    db_conn.close()
    
    # Inicia el servicio para responder los Broadcasts
    listen_broadcast_thread = ListenBroadcastThread()
    listen_broadcast_thread.daemon = True
    listen_broadcast_thread.start()
    
    # Inicia el servicio de consulta de cotizacion
    consulta_cotizacion_thread = ConsultaCotizacionThread()
    consulta_cotizacion_thread.daemon = True
    consulta_cotizacion_thread.start()

    # Codigo para el uso de argumentos por linea de comandos
    parser = argparse.ArgumentParser(description='Servidor')

    parser.add_argument('-p', 
                        default=TCP_PORT, 
                        type=int, 
                        help='Puerto del servicio')

    args = parser.parse_args()
    tcp_port = args.p


    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((TCP_IP, tcp_port))
    server_socket.listen(5)
    
    while True:
        (client_socket, address) = server_socket.accept()
        client_thread = ClientThread(client_socket, address)
        client_thread.daemon = True
        client_thread.start()

except OSError as ex:
    print('No se puede asignar este proceso al puerto ' + str(tcp_port))
    
    logging.exception(traceback.format_exc())
    
except KeyboardInterrupt as ex:
    print()
    print('Terminacion manual con Ctrl-c')
    
finally:
    server_socket.close()
